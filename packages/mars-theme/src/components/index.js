import { Global, css, connect, styled, Head } from "frontity";
import Switch from "@frontity/components/switch";
import Header from "./header";
import List from "./list";
import Post from "./post";
import Loading from "./loading";
import Title from "./title";
import PageError from "./page-error";
import ElementorStyles from "./frontend.min.css"
import ElementorGlobalStyles from "./global.css"
import Post3 from "./post-2.css"
import Post7 from "./post-7.css"
 import Post2 from "./post-3.css"
import Post833 from "./post-833.css"
import Post3913 from "./post-913.css"
import Post1146 from "./post-1146.css"
import Post2908 from "./post-2908.css"
import Post2952 from "./post-2952.css"

/**
 * Theme is the root React component of our theme. The one we will export
 * in roots.
 *
 * @param props - The props injected by Frontity's {@link connect} HOC.
 *
 * @returns The top-level react component representing the theme.
 */
const Theme = ({ state }) => {
  // Get information about the current URL.
  const data = state.source.get(state.router.link);

  return (
    <>
      {/* Add some metatags to the <head> of the HTML. */}
      <Title />
      <Head bodyAttributes={{
        class: 'page-template-default page page-id-7 logged-in admin-bar wp-custom-logo group-blog ast-single-post ast-inherit-site-logo-transparent ast-hfb-header ast-desktop ast-page-builder-template ast-no-sidebar astra-3.9.4 ast-full-width-primary-header elementor-default elementor-kit-3 elementor-page elementor-page-7 astra-addon-3.5.4 customize-support e--ua-blink e--ua-chrome e--ua-mac e--ua-webkit'
      }} >
        <meta name="description" content={state.frontity.description} />
        <html lang="en" />
      </Head>

      {/* Add some global styles for the whole site, like body or a's. 
      Not classes here because we use CSS-in-JS. Only global HTML tags. */}
      <Global styles={globalStyles} />
      <Global styles={css(ElementorStyles)} />
      <Global styles={css(ElementorGlobalStyles)} />
      {/* <Global styles={css(Post3)} /> */}
      {/* <Global styles={css(Post7)} /> */}
      <Global styles={css(Post2)} />
      {/* <Global styles={css(Post3913)} /> */}
      {/* <Global styles={css(Post1146)} /> */}
      {/* <Global styles={css(Post2908)} /> */}
      {/* <Global styles={css(Post833)} /> */}

      {/* Add the header of the site. */}
      <HeadContainer>
        <Header />
      </HeadContainer>

      {/* Add the main section. It renders a different component depending
      on the type of URL we are in. */}
      <Main>
        <Switch>
          <Loading when={data.isFetching} />
          <List when={data.isArchive} />
          <Post when={data.isPostType} />
          <PageError when={data.isError} />
        </Switch>
      </Main>
    </>
  );
};

export default connect(Theme);

const globalStyles = css`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
      "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  }
  a,
  a:visited {
    color: inherit;
    text-decoration: none;
  }
`;

const HeadContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: #1f38c5;
`;

const Main = styled.div`
  display: flex;
  justify-content: center;
  background-image: linear-gradient(
    180deg,
    rgba(66, 174, 228, 0.1),
    rgba(66, 174, 228, 0)
  );
`;
